[out:json][timeout:25];

// Ottieni e metti in output tutte le aree di attesa nel bounding box
// {{bbox}} = <latitudine min>,<longitudine min>,<lat max>,<lon max>
(
  node["emergency"="assembly_point"](44.48655583488967,11.311197280883789,44.49589310341622,11.32965087890625);
  way["emergency"="assembly_point"](44.48655583488967,11.311197280883789,44.49589310341622,11.32965087890625);
);
out body;

// Ottieni e metti in output i nodi che compongono le way selezionate
>;
out skel qt;
