GoogleMap map = ...
Area area = ...
Nodo n = (Nodo) area.getGeometria();
LatLng pos = new LatLng(n.getLatitudine(), n.getLongitudine());
BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.attesa);
MarkerOptions mOpt = new MarkerOptions()
                        .draggable(false)
                        .position(pos)
                        .icon(icon)
                        .title(area.getNome());
Marker m = map.addMarker(mOpt);
m.setTag(area);

