package eu.danielesantini.areediattesa.async;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class ImageToSet {
    private ImageView view;
    private Drawable drawable;

    public ImageToSet(ImageView view, Drawable drawable) {
        this.view = view;
        this.drawable = drawable;
    }

    public ImageView getView() {
        return view;
    }

    public Drawable getDrawable() {
        return drawable;
    }
}
