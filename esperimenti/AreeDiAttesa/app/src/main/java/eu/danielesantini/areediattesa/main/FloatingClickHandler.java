package eu.danielesantini.areediattesa.main;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;

public class FloatingClickHandler implements View.OnClickListener {
    private MapboxMap map;
    private Activity context;

    private static final String TAG = "FloatingClickHandler";
    public static final int ASK_POS = 1;

    public FloatingClickHandler(Activity context, MapboxMap map) {
        this.map = map;
        this.context = context;
    }

    @Override
    public void onClick(View view) {
        //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        boolean locationDisabled = !map.isMyLocationEnabled();

        Log.v(TAG,"Controllo autorizzazione posizione");
        if (locationDisabled &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG,"Richiesta autorizzazione posizione");
            ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ASK_POS);
        }


        Log.d(TAG,"Commutazione vista posizione");
        map.setMyLocationEnabled(locationDisabled);
    }
}
