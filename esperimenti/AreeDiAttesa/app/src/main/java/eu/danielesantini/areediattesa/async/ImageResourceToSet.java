package eu.danielesantini.areediattesa.async;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.widget.ImageView;

public class ImageResourceToSet {
    private ImageView view;
    private int imgId;
    private Resources resources;

    public ImageResourceToSet(ImageView view, Resources resources, int imgId) {
        this.view = view;
        this.imgId = imgId;
        this.resources = resources;
    }

    public ImageView getView() {
        return view;
    }

    public int getImgId() {
        return imgId;
    }

    public Resources getResources() {
        return resources;
    }
}
