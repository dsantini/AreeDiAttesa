package eu.danielesantini.areediattesa.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.mapbox.mapboxsdk.maps.MapboxMap;

public class PermissionCallback implements ActivityCompat.OnRequestPermissionsResultCallback {
    private MapboxMap map;

    private static final String TAG = "PermissionCallback";

    public PermissionCallback(MapboxMap map) {
        this.map = map;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.v(TAG,"Controllo permesso concesso");
        if (requestCode == FloatingClickHandler.ASK_POS &&
                Manifest.permission.ACCESS_FINE_LOCATION.equals(permissions[0]) &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED)

            Log.d(TAG,"Commutazione vista posizione");
            map.setMyLocationEnabled(true);
    }
}
