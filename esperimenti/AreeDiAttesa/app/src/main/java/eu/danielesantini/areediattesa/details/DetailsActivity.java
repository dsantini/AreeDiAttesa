package eu.danielesantini.areediattesa.details;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import eu.danielesantini.areediattesa.async.AsyncImageSetter;
import eu.danielesantini.areediattesa.async.ImageResourceToSet;
import eu.danielesantini.areediattesa.main.MapClickHandler;
import eu.danielesantini.areediattesa.R;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {
    private double lat, lon;
    private static final String TAG = "DetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();

        lat = intent.getDoubleExtra(MapClickHandler.LAT, 0);
        lon = intent.getDoubleExtra(MapClickHandler.LON, 0);

        String name = null;
        if (intent.hasExtra(MapClickHandler.NAME)) {
            name = intent.getStringExtra(MapClickHandler.NAME);
            setTitle(name);
        }
        if (intent.hasExtra(MapClickHandler.DESCRIPTION))
            ((TextView) findViewById(R.id.desc)).setText(intent.getStringExtra(MapClickHandler.DESCRIPTION));
        if (intent.hasExtra(MapClickHandler.REF)) {
            final Integer ref = intent.getIntExtra(MapClickHandler.REF, -1);
            ((TextView) findViewById(R.id.ref)).setText(String.format(Locale.getDefault(), "#%d (lat:%f lon:%f)", ref, lat, lon));

            int imgId = getResources().getIdentifier("punto" + ref, "drawable", getApplicationContext().getPackageName());
            if (imgId == 0)
                Log.d(TAG,"Immagine non dispoinibile per " + name);
            else {
                Log.d(TAG,"Caricamento immagine per " + name);
                //((ImageView) findViewById(R.id.img)).setImageResource(imgId);

                (new AsyncImageSetter()).execute(new ImageResourceToSet((ImageView)findViewById(R.id.img), getResources(), imgId));
            }
            //// class Xyz extends AsyncTask<...> {...}
            //// (new Xyz()).execute(...);
        }
        //if(intent.hasExtra(MainActivity.IMAGE)){...}

        findViewById(R.id.nav).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + lat + "," + lon));
        //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:q="+lat+","+lon));
        //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:"+lat+","+lon));
        startActivity(intent);
    }
}
