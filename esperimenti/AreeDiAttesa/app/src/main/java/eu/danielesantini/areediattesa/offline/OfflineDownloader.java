package eu.danielesantini.areediattesa.offline;

import android.util.Log;

import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;

public class OfflineDownloader implements OfflineManager.CreateOfflineRegionCallback {
    private static final String TAG = "OfflineDownloader";

    @Override
    public void onCreate(OfflineRegion offlineRegion) {
        offlineRegion.setDownloadState(OfflineRegion.STATE_ACTIVE);

        // Monitor the download progress using setObserver
        offlineRegion.setObserver(new OfflineObserver());
    }

    @Override
    public void onError(String error) {
        Log.e(TAG, "Error: " + error);
    }
}
