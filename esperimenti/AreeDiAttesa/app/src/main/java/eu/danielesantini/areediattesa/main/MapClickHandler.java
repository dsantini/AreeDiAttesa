package eu.danielesantini.areediattesa.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.support.annotation.NonNull;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.services.commons.geojson.Feature;
import com.mapbox.services.commons.geojson.Geometry;
import com.mapbox.services.commons.models.Position;

import java.util.List;

import eu.danielesantini.areediattesa.details.DetailsActivity;

public class MapClickHandler implements MapboxMap.OnMapClickListener {
    private MapboxMap mapboxMap;
    private Context context;

    public static final String REF = "ref",
            NAME = "name",
            DESCRIPTION = "description",
            IMAGE = "image",
            LAT = "lat",
            LON = "lon";

    public MapClickHandler(Context context, MapboxMap mapboxMap) {
        this.context = context;
        this.mapboxMap = mapboxMap;
    }

    @Override
    public void onMapClick(@NonNull LatLng point) {
        final PointF pixel = mapboxMap.getProjection().toScreenLocation(point);
        List<Feature> features = mapboxMap.queryRenderedFeatures(pixel, MapBuilder.PUNTI);
        if (!features.isEmpty()) {
            Feature punto = features.get(0);
            Intent intent = new Intent(context, DetailsActivity.class);

            //Log.i("onMapClick",punto.toJson());
            Geometry g = punto.getGeometry();
            Position p = (Position) g.getCoordinates();
            intent.putExtra(LAT, p.getLatitude());
            intent.putExtra(LON, p.getLongitude());

            if (punto.hasNonNullValueForProperty(REF))
                intent.putExtra(REF, punto.getNumberProperty(REF).intValue());
            if (punto.hasNonNullValueForProperty(NAME))
                intent.putExtra(NAME, punto.getStringProperty(NAME));
            if (punto.hasNonNullValueForProperty(DESCRIPTION))
                intent.putExtra(DESCRIPTION, punto.getStringProperty(DESCRIPTION));
            if (punto.hasNonNullValueForProperty(IMAGE))
                intent.putExtra(IMAGE, punto.getStringProperty(IMAGE));

            context.startActivity(intent);
        }
    }
}
