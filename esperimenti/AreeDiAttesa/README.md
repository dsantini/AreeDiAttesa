# AreeDiAttesa
App per vedere i punti attesa del piano di emergenza comunale di Imola

Basato su OpenStreetMap tramite Overpass e Mapbox Maps SDK.

![Zoom basso](Screen1.png)
![Zoom alto](Screen2.png)
![Dettagli](Screen3.png)