Per utilizzare l'attività con Google Maps è necessario inserire la chiave dell'API in `app/src/main/res/values/google_maps_api.xml` al posto di `API_KEY`.

La chiave è ottenibile da https://console.developers.google.com/flows/enableapi?apiid=maps_android_backend&keyType=CLIENT_SIDE_ANDROID&r=C1:2C:9D:DC:75:6E:20:D8:36:53:1F:99:9F:D7:C2:02:2E:83:2A:FE%3Bit.dsantini.areediattesa

