package it.dsantini.areediattesa.model;

import junit.framework.Assert;

import org.junit.Test;

import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.impl.BoundingBoxBase;

public class BoundingBoxBaseTest {
    private final Confine contenuto, contenutoClone, contenente;

    public BoundingBoxBaseTest() {
        contenuto = new BoundingBoxBase(1, 2, 0, 1, "a");
        contenutoClone = new BoundingBoxBase(1, 2, 0, 1, "a");
        contenente = new BoundingBoxBase(0, 3, -1, 2, "b");
    }

    public void nomeNull() {
        Confine c = new BoundingBoxBase(0, 1, 0, 1, null);
        Assert.assertFalse(c.getNome().isPresent());
    }

    public void nomeNonNull() {
        Confine c = new BoundingBoxBase(0, 1, 0, 1, "Wololoooo");
        Assert.assertTrue(c.getNome().isPresent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void latUguale() {
        new BoundingBoxBase(0, 0, 0, 1, "a");
    }

    @Test(expected = IllegalArgumentException.class)
    public void lonUguale() {
        new BoundingBoxBase(0, 1, 0, 0, "a");
    }

    @Test(expected = IllegalArgumentException.class)
    public void latInvertita() {
        new BoundingBoxBase(1, 0, 0, 1, "a");
    }

    @Test
    public void lonInvertita() {
        new BoundingBoxBase(0, 1, 1, 0, "a");
    }

    @Test
    public void latTotale() {
        new BoundingBoxBase(-90, 90, 0, 1, "a");
    }

    @Test
    public void lonTotale() {
        new BoundingBoxBase(0, 1, -180, 180, "a");
    }

    @Test(expected = IllegalArgumentException.class)
    public void latAlta() {
        new BoundingBoxBase(0, 91, 0, 1, "a");
    }

    @Test(expected = IllegalArgumentException.class)
    public void latBassa() {
        new BoundingBoxBase(-91, 0, 0, 1, "a");
    }

    @Test(expected = IllegalArgumentException.class)
    public void lonAlta() {
        new BoundingBoxBase(0, 1, 0, 181, "a");
    }

    @Test(expected = IllegalArgumentException.class)
    public void lonBassa() {
        new BoundingBoxBase(0, 1, -181, 0, "a");
    }

    @Test(expected = IllegalArgumentException.class)
    public void contieneNull() {
        contenuto.contiene(null);
    }

    @Test
    public void contieneUguale() {
        Assert.assertTrue(contenuto.contiene(contenuto));
    }

    @Test
    public void contieneSemplice() {
        Assert.assertTrue(contenente.contiene(contenuto));
    }

    @Test
    public void contenutoSemplice() {
        Assert.assertFalse(contenuto.contiene(contenente));
    }

    @Test
    public void contenutoUguale() {
        Assert.assertTrue(contenuto.contiene(contenutoClone));
        Assert.assertTrue(contenutoClone.contiene(contenuto));
    }
}
