package it.dsantini.areediattesa.persistenza;

import android.support.annotation.NonNull;

import org.junit.Test;

import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.model.impl.MappaBase;

public abstract class PersistenzaTest {
    @NonNull
    private final Persistenza p;

    public PersistenzaTest(@NonNull Persistenza p) {
        this.p = p;
    }

    @Test
    public void caricaMappa() {
        p.carica();
    }

    @Test
    public void salvaMappaVuota() {
        Mappa m = new MappaBase(new GruppoAree[0]);
        p.salva(m);
    }
}
