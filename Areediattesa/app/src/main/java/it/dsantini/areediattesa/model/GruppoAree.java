package it.dsantini.areediattesa.model;

import android.support.annotation.NonNull;

import java.time.LocalDateTime;

public interface GruppoAree {
    /**
     * @return Aree di emergenza contenute nel gruppo di aree
     */
    @NonNull
    AreaEmergenza[] getAree();

    /**
     * @return Confine a cui appartengono le aree
     */
    @NonNull
    Confine getConfine();

    /**
     * @return Se il gruppo è fra i preferiti dell'utente
     */
    boolean isPreferito();

    /**
     * @param p nuovo valore di preferito
     * @return Clone del gruppo, con preferito modificato
     */
    @NonNull
    GruppoAree withPreferito(boolean p);

    /**
     * @return Momento in cui è stato scaricato il gruppo di aree
     */
    @NonNull
    LocalDateTime getTimestamp();
}
