package it.dsantini.areediattesa.viewModel.helper.mock;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.impl.AreaAttesa;
import it.dsantini.areediattesa.view.DettagliView;
import it.dsantini.areediattesa.viewModel.helper.AreaViewMapping;

public class AreaViewMappingMock implements AreaViewMapping {
    @NonNull
    @Override
    public Map<Class<? extends AreaEmergenza>, Class<? extends DettagliView>> getMap() {
        Map<Class<? extends AreaEmergenza>, Class<? extends DettagliView>> m = new HashMap<>();
        m.put(AreaAttesa.class, DettagliView.class);
        return m;
    }
}
