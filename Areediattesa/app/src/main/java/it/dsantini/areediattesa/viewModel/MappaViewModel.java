package it.dsantini.areediattesa.viewModel;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.data.Stato;
import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Confine;

public interface MappaViewModel {
    /***
     * @return Aree di emergenza osservabili da visualizzare
     */
    @NonNull
    LiveData<AreaEmergenza[]> getAree();

    /***
     * @return Stato osservabile della mappa
     */
    @NonNull
    LiveData<Stato> getStato();

    /***
     * @return Stato osservabile dello scaricamento
     */
    @NonNull
    LiveData<StatoDownload> getStatoDownload();

    /***
     * @param porzioneDiMappa Porzione di mappa su cui spostarsi
     */
    void onSposta(@NonNull Confine porzioneDiMappa);

    /***
     * @param a Area di emergenza di cui visualizzare i dettagli
     */
    void onScegliArea(@NonNull AreaEmergenza a);


}
