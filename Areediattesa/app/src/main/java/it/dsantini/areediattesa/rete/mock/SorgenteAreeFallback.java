package it.dsantini.areediattesa.rete.mock;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.BoundingBox;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.rete.DownloadException;
import it.dsantini.areediattesa.rete.impl.SorgenteAreeBase;

public class SorgenteAreeFallback extends SorgenteAreeBase {

    @NonNull
    @Override
    public GruppoAree visit(@NonNull BoundingBox b) throws DownloadException {
        throw new UnsupportedOperationException("Download non implementato (Sorgente aree fallback)");
    }
}
