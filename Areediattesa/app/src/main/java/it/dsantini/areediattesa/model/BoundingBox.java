package it.dsantini.areediattesa.model;

public interface BoundingBox extends Confine {
    /**
     * @return Latitudine minima (limite sud)
     */
    double getLatMin();

    /**
     * @return Latitudine massima (limite nord)
     */
    double getLatMax();

    /**
     * @return Longitudine minima (limite ovest)
     */
    double getLonMin();

    /**
     * @return Longitudine massima (limite est)
     */
    double getLonMax();
}
