package it.dsantini.areediattesa.model.impl.googleMaps;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

import it.dsantini.areediattesa.model.impl.NodoBase;

public class NodoLatLng extends NodoBase {
    public NodoLatLng(@NonNull LatLng ll) {
        super(ll.latitude, ll.longitude);
    }
}
