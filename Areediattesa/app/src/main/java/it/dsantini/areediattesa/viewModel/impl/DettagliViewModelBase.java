package it.dsantini.areediattesa.viewModel.impl;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import it.dsantini.areediattesa.data.DettagliRepository;
import it.dsantini.areediattesa.data.impl.DettagliRepositoryBase;
import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Nodo;
import it.dsantini.areediattesa.viewModel.DettagliViewModel;
import it.dsantini.areediattesa.viewModel.StatoDettagli;

public class DettagliViewModelBase extends AndroidViewModel implements DettagliViewModel {
    private static final String TAG = "DettagliViewModelBase";

    @NonNull
    private final DettagliRepository d;
    @NonNull
    private final MutableLiveData<StatoDettagli> stato;
    @NonNull
    private final NumberFormat format;

    public DettagliViewModelBase(@NonNull Application application) {
        super(application);
        d = DettagliRepositoryBase.getInstance();
        stato = new MutableLiveData<>();
        stato.setValue(StatoDettagli.pronto);

        format = DecimalFormat.getNumberInstance(Locale.US);
    }

    @NonNull
    @Override
    public LiveData<AreaEmergenza> getArea() {
        return d.getArea();
    }

    @NonNull
    @Override
    public LiveData<StatoDettagli> getStato() {
        return stato;
    }

    @Override
    public void onAvviaNavigazione() {
        AreaEmergenza a = getArea().getValue();
        if (a == null) {
            Log.e(TAG, "Richiesta navigazione ma area nulla");
            stato.setValue(StatoDettagli.navigazione_errore);
        } else {
            try {
                Nodo n = a.getGeometria().getPuntoSuSuperficie();

                String query = "google.navigation:q="
                        + format.format(n.getLatitudine())
                        + ","
                        + format.format(n.getLongitudine());

                Log.e(TAG, query);

                Uri uri = Uri.parse(query);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                getApplication().startActivity(intent);
            } catch (ActivityNotFoundException e) {
                stato.setValue(StatoDettagli.navigazione_nessuna_app);
                Log.e(TAG, "Nessuna app disponibile per la navigazione", e);
            } catch (Exception e) {
                stato.setValue(StatoDettagli.navigazione_errore);
                Log.e(TAG, "Errore durante l'avvio della navigazione", e);
            }
        }
    }
}
