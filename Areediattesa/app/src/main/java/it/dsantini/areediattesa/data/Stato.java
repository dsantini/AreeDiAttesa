package it.dsantini.areediattesa.data;

public enum Stato {
    non_caricato,
    caricamento,
    errore_caricamento,
    caricato,
    salvataggio,
    errore_salvataggio;
}
