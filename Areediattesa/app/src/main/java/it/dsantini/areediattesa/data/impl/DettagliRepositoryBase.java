package it.dsantini.areediattesa.data.impl;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.data.DettagliRepository;
import it.dsantini.areediattesa.model.AreaEmergenza;

public class DettagliRepositoryBase implements DettagliRepository {
    private static DettagliRepositoryBase instance;

    @NonNull
    private final MutableLiveData<AreaEmergenza> area;

    private DettagliRepositoryBase() {
        area = new MutableLiveData<>();
    }

    public static DettagliRepositoryBase getInstance() {
        if (instance == null)
            instance = new DettagliRepositoryBase();

        return instance;
    }

    @NonNull
    @Override
    public LiveData<AreaEmergenza> getArea() {
        return area;
    }

    @Override
    public void setArea(AreaEmergenza a) {
        area.setValue(a);
    }
}
