package it.dsantini.areediattesa.model.impl;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Optional;

import it.dsantini.areediattesa.model.Fotografia;

@Entity
public class FotografiaBase implements Fotografia {
    @PrimaryKey
    private final int id;
    public static final int DEFAULT_ID = Integer.MIN_VALUE;

    @NonNull
    private final Drawable immagine;
    @Nullable
    private final String titolo;

    public FotografiaBase(int id, @NonNull Drawable immagine, @Nullable String titolo) {
        if (immagine == null)
            throw new IllegalArgumentException("immagine == null");

        this.id = id;
        this.immagine = immagine;
        this.titolo = titolo;
    }

    @Ignore
    public FotografiaBase(@NonNull Drawable immagine, @Nullable String titolo) {
        this(DEFAULT_ID, immagine, titolo);
    }

    public int getId() {
        return id;
    }

    @NonNull
    @Override
    public Drawable getImmagine() {
        return immagine;
    }

    @NonNull
    @Override
    public Optional<String> getTitolo() {
        return Optional.ofNullable(titolo);
    }
}
