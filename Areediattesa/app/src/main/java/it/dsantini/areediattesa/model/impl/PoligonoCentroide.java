package it.dsantini.areediattesa.model.impl;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.Nodo;

public class PoligonoCentroide extends Poligono {

    public PoligonoCentroide(@NonNull Nodo[] vertici) {
        super(vertici);
    }

    @Override
    @NonNull
    public Nodo getPuntoSuSuperficie() {
        double lat = 0, lon = 0;
        for (Nodo n : getVertici()) {
            lat += n.getLatitudine();
            lon += n.getLongitudine();
        }
        int num = getVertici().length;
        return new NodoBase(lat / num, lon / num);
        // TODO Funzione attualmente non corretta: questo è il centroide, che non è garantito sia sulla superficie
    }
}
