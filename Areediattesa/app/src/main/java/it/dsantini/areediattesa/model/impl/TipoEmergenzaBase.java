package it.dsantini.areediattesa.model.impl;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.TipoEmergenza;

@Entity
public class TipoEmergenzaBase implements TipoEmergenza {
    public static final TipoEmergenza TERREMOTO = new TipoEmergenzaBase("Terremoto"),
            ALLUVIONE = new TipoEmergenzaBase("Alluvione"),
            INCENDIO = new TipoEmergenzaBase("Incendio");

    @PrimaryKey
    @NonNull
    private final String nome;

    public TipoEmergenzaBase(@NonNull String nome) {
        if (nome == null)
            throw new IllegalArgumentException("nome == null");

        if (nome.isEmpty())
            throw new IllegalArgumentException("nome vuoto");

        this.nome = nome;
    }


    @Override
    @NonNull
    public String getNome() {
        return nome;
    }
}
