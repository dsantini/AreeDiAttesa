package it.dsantini.areediattesa.view.helper.impl;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import it.dsantini.areediattesa.model.BoundingBox;
import it.dsantini.areediattesa.view.helper.ConfinePainter;

public class GoogleMapConfinePainter implements ConfinePainter {
    @NonNull
    private final GoogleMap map;
    @Nullable
    private final Object tag;

    public GoogleMapConfinePainter(@NonNull GoogleMap map) {
        this(map, null);
    }

    public GoogleMapConfinePainter(@NonNull GoogleMap map, @Nullable Object tag) {
        if (map == null)
            throw new IllegalArgumentException("GoogleMap nulla");

        this.map = map;
        this.tag = tag;
    }

    @Override
    public void stampa(@NonNull BoundingBox box) {
        PolygonOptions pOpt = new PolygonOptions()
                .fillColor(Color.argb(100, 0, 255, 0))
                .clickable(true);
        pOpt.add(new LatLng(box.getLatMin(), box.getLonMin()),
                new LatLng(box.getLatMin(), box.getLonMax()),
                new LatLng(box.getLatMax(), box.getLonMax()),
                new LatLng(box.getLatMax(), box.getLonMin()));
        Polygon gmp = map.addPolygon(pOpt);
        if(tag!=null)
            gmp.setTag(tag);
        else
            gmp.setTag(box);
    }
}
