package it.dsantini.areediattesa.model.impl.googleMaps;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLngBounds;

import it.dsantini.areediattesa.model.impl.BoundingBoxBase;

public class BoundingBoxLatLngBounds extends BoundingBoxBase {

    public BoundingBoxLatLngBounds(@NonNull LatLngBounds bounds, @Nullable String nome) {
        super(bounds.southwest.latitude, bounds.northeast.latitude, bounds.southwest.longitude, bounds.northeast.longitude, nome);
    }
}
