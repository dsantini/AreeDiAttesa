package it.dsantini.areediattesa.model.impl;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.Nodo;
import it.dsantini.areediattesa.view.helper.GeometriaPainter;

@Entity(primaryKeys = {"latitudine", "longitudine"})
public class NodoBase implements Nodo {
    private final double latitudine, longitudine;

    public NodoBase(double latitudine, double longitudine) {
        if (latitudine < -90)
            throw new IllegalArgumentException("latitudine = " + latitudine + " < -90");
        if (latitudine > 90)
            throw new IllegalArgumentException("latitudine = " + latitudine + " > 90");
        if (longitudine < -180)
            throw new IllegalArgumentException("longitudine = " + longitudine + " < -180");
        if (longitudine > 180)
            throw new IllegalArgumentException("longitudine = " + longitudine + " > 180");

        Double Latitudine = latitudine, Longitudine = longitudine;
        if (Latitudine.isNaN())
            throw new IllegalArgumentException("latitudine == NaN");
        if (Latitudine.isInfinite())
            throw new IllegalArgumentException("latitudine == infinto");
        if (Longitudine.isNaN())
            throw new IllegalArgumentException("longitudine == NaN");
        if (Longitudine.isInfinite())
            throw new IllegalArgumentException("longitudine == infinito");

        this.latitudine = latitudine;
        this.longitudine = longitudine;
    }

    @Override
    public double getLatitudine() {
        return latitudine;
    }

    @Override
    public double getLongitudine() {
        return longitudine;
    }

    @NonNull
    @Override
    public Nodo getPuntoSuSuperficie() {
        return this;
    }

    @Override
    public void accept(GeometriaPainter gp) {
        gp.stampa(this);
    }

    @Override
    public String toString() {
        return "NodoBase, lat=" + getLatitudine() + ", lon=" + getLongitudine();
    }
}
