package it.dsantini.areediattesa.view.helper.impl;

import android.graphics.Color;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import it.dsantini.areediattesa.R;
import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Nodo;
import it.dsantini.areediattesa.model.impl.Poligono;
import it.dsantini.areediattesa.view.helper.GeometriaPainter;

public class GoogleMapAreaGeometriaPainter implements GeometriaPainter {
    private static BitmapDescriptor icon;

    @NonNull
    private final GoogleMap map;
    @NonNull
    private final AreaEmergenza area;

    public GoogleMapAreaGeometriaPainter(@NonNull GoogleMap map, @NonNull AreaEmergenza area) {
        this.map = map;
        this.area = area;

        if (icon == null)
            icon = BitmapDescriptorFactory.fromResource(R.drawable.attesa);
    }

    @Override
    public void stampa(@NonNull Nodo n) {
        LatLng pos = new LatLng(n.getLatitudine(), n.getLongitudine());
        MarkerOptions mOpt = new MarkerOptions()
                .draggable(false)
                .position(pos)
                .icon(icon)
                .title(area.getNome());
        Marker m = map.addMarker(mOpt);
        m.setTag(area);
    }

    @Override
    public void stampa(@NonNull Poligono p) {
        PolygonOptions pOpt = new PolygonOptions()
                .fillColor(Color.argb(100, 0, 255, 0))
                .clickable(true);
        for (Nodo n : p.getVertici())
            pOpt.add(new LatLng(n.getLatitudine(), n.getLongitudine()));
        Polygon gmp = map.addPolygon(pOpt);
        gmp.setTag(area);
    }
}
