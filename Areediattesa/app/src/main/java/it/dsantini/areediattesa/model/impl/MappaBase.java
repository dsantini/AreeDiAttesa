package it.dsantini.areediattesa.model.impl;

import android.support.annotation.NonNull;
import android.util.Log;

import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;

public class MappaBase implements Mappa {
    private static final String TAG = "MappaBase";

    @NonNull
    private final GruppoAree[] gruppi;

    public MappaBase(@NonNull GruppoAree[] gruppi) {
        if (gruppi == null)
            throw new IllegalArgumentException("gruppi == null");

        for (int i = 0; i < gruppi.length; i++)
            if (gruppi[i] == null)
                throw new IllegalArgumentException("gruppi[" + i + "] == null");

        for (int i = 0; i < gruppi.length; i++)
            for (int j = 0; i < gruppi.length; i++)
                if (i != j && gruppi[i] == gruppi[j])
                    throw new IllegalArgumentException("gruppi[" + i + "] == gruppi[" + j + "]");

        this.gruppi = gruppi;
    }

    @Override
    @NonNull
    public GruppoAree[] getGruppi() {
        return gruppi;
    }

    @NonNull
    @Override
    public Mappa con(@NonNull GruppoAree g) {
        if (g == null)
            throw new IllegalArgumentException("GruppoAree g nullo");

        GruppoAree[] nuovo = new GruppoAree[gruppi.length + 1];
        System.arraycopy(gruppi, 0, nuovo, 0, gruppi.length);
        nuovo[gruppi.length] = g;
        return new MappaBase(nuovo);
    }

    @NonNull
    @Override
    public Mappa senza(@NonNull GruppoAree g) {
        if (g == null)
            throw new IllegalArgumentException("GruppoAree g nullo");

        int pos = -1;
        for (int i = 0; i < gruppi.length; i++) {
            if (gruppi[i] == g) {
                pos = i;
                break;
            }
        }

        if (pos < 0) { // Gruppo non presente
            Log.e(TAG, "Gruppo da eliminare non presente");
            return this;
        } else {
            GruppoAree[] nuovo = new GruppoAree[gruppi.length - 1];
            System.arraycopy(gruppi, 0, nuovo, 0, pos);
            if (gruppi.length - 1 - pos > 0)
                System.arraycopy(gruppi, pos + 1, nuovo, pos, gruppi.length - 1 - pos);
            return new MappaBase(nuovo);
        }
    }

    @Override
    public String toString() {
        return "Mappa con " + getGruppi().length + " gruppi";
    }
}
