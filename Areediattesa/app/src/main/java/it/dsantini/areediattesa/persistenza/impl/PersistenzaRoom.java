package it.dsantini.areediattesa.persistenza.impl;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.model.impl.MappaBase;
import it.dsantini.areediattesa.persistenza.Persistenza;

public class PersistenzaRoom implements Persistenza {
    @NonNull
    private final MappaDatabase db;

    public PersistenzaRoom(@NonNull Context c) {
        db = Room.databaseBuilder(c, MappaDatabase.class, "aree-di-attesa").build();
    }

    @Override
    public void salva(@NonNull Mappa m) {
        throw new UnsupportedOperationException(); // TODO
    }

    @NonNull
    @Override
    public Mappa carica() {
        GruppoAreeDao gad = db.gruppoAreeDao();
        Mappa mappa = new MappaBase(gad.loadAllGruppi());
        return mappa;

        //throw new UnsupportedOperationException(); // TODO
    }
}
