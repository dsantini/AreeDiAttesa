package it.dsantini.areediattesa.model;

import android.support.annotation.NonNull;

import java.util.Optional;

import it.dsantini.areediattesa.rete.DownloadException;
import it.dsantini.areediattesa.rete.SorgenteAree;
import it.dsantini.areediattesa.view.helper.ConfinePainter;

public interface Confine {
    /**
     * @return Nome del confine
     */
    @NonNull
    Optional<String> getNome();

    /**
     * Pattern Visitor per il confronto fra confini (funzione accept)
     * Implementazione tipica: return contenuto.isContenuto(this)
     *
     * @param contenuto Il confine da confronatare
     * @return Se il confine specificato è contenuto in questo
     */
    boolean contiene(@NonNull Confine contenuto);

    /**
     * Pattern Visitor per il confronto fra confini (funzione visit)
     *
     * @param contenente Il confine da confrontare
     * @return Se il confine specificato contiene questo
     */
    boolean isContenuto(@NonNull BoundingBox contenente);

    /**
     * Pattern Visitor per download gruppo di aree dato il confine (funzione accept)
     * Implementazione tipica: return s.visit(this)
     *
     * @param s Sorgente da cui scaricare il gruppo di aree
     * @return Gruppo di aree scaricato
     */
    @NonNull
    GruppoAree accept(@NonNull SorgenteAree s) throws DownloadException;

    /**
     * Pattern Visitor per la stampa del confine (funzione accept)
     * Implementazione tipica: return cp.stampa(this)
     *
     * @param cp Painter con cui stampare il confine
     */
    void accept(@NonNull ConfinePainter cp);
}
