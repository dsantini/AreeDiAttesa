package it.dsantini.areediattesa.viewModel.impl;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import it.dsantini.areediattesa.data.MappaRepository;
import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.rete.DownloadException;
import it.dsantini.areediattesa.rete.SorgenteAree;
import it.dsantini.areediattesa.viewModel.StatoDownload;

public class DownloadAsync extends Thread {
    private static final String CLASS = "DownloadAsync";

    public DownloadAsync(@NonNull final MutableLiveData<StatoDownload> stato,
                         @NonNull final SorgenteAree sorgente,
                         @NonNull final Confine confine,
                         @NonNull final MappaRepository repo) {
        super(new Runnable() {
            @Override
            public void run() {
                Mappa vecchiaMappa = repo.getMappa().getValue();
                if (vecchiaMappa == null) {
                    stato.postValue(StatoDownload.scaricamento_fallito);
                    Log.e(CLASS, "Download fallito (mappa nulla)");
                } else {
                    // Attendere finchè gli scaricamenti precedenti sono completati
                        /*
                        while (stato.getValue() == StatoDownload.scaricamento_in_corso) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                Log.w(CLASS, e);
                            }
                        }
                        */

                    stato.postValue(StatoDownload.scaricamento_in_corso);
                    try {
                        GruppoAree nuovo = sorgente.download(confine);

                        Mappa nuovaMappa = vecchiaMappa;

                        // Rimuovi gruppi vecchi contenuti dentro al nuovo gruppo
                        for (GruppoAree vecchio : vecchiaMappa.getGruppi())
                            if (nuovo.getConfine().contiene(vecchio.getConfine()))
                                nuovaMappa = nuovaMappa.senza(vecchio);

                        // Aggiungi il nuovo gruppo
                        nuovaMappa = nuovaMappa.con(nuovo);

                        // Aggiorna la mappa nella repository
                        repo.postMappa(nuovaMappa);
                        stato.postValue(StatoDownload.scaricato);
                    } catch (DownloadException e) {
                        stato.postValue(StatoDownload.scaricamento_fallito);
                        Log.e(CLASS, "Download fallito (errore nel download)", e);
                    } catch (Exception e) {
                        stato.postValue(StatoDownload.scaricamento_fallito);
                        Log.e(CLASS, "Download fallito (errore imprevisto)", e);
                    }
                }
            }
        });
    }
}
