package it.dsantini.areediattesa.viewModel.helper.impl;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import it.dsantini.areediattesa.R;
import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.view.DettagliView;
import it.dsantini.areediattesa.viewModel.helper.AreaViewMapping;

public class AreaViewMappingConfig implements AreaViewMapping {
    private static final String TAG = "AreaViewMappingConfig";

    @NonNull
    private final Map<Class<? extends AreaEmergenza>, Class<? extends DettagliView>> map;

    public AreaViewMappingConfig(@NonNull Context context) {
        map = new HashMap<>();

        Resources r = context.getResources();
        String[] mapping = r.getStringArray(R.array.areaViewMapping);
        for (String s : mapping) {
            Log.v(TAG, s);
            String[] split = s.split("-");
            if (split.length == 2) {
                try {
                    Class<? extends AreaEmergenza> area = Class.forName(split[0]).asSubclass(AreaEmergenza.class);
                    Class<? extends DettagliView> view = Class.forName(split[1]).asSubclass(DettagliView.class);
                    map.put(area, view);
                } catch (ClassNotFoundException e) {
                    Log.e(TAG, "Mappatura area-view sbagliata (classe non trovata): " + split[0] + " -> " + split[1], e);
                } catch (ClassCastException e) {
                    Log.e(TAG, "Mappatura area-view sbagliata (classe non accettabile): " + split[0] + " -> " + split[1], e);
                }
            }
        }
    }

    @Override
    @NonNull
    public Map<Class<? extends AreaEmergenza>, Class<? extends DettagliView>> getMap() {
        return map;
    }
}
